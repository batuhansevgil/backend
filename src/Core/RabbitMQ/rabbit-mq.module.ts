import { Global, Module } from '@nestjs/common';
import { RabbitMqService } from './rabbit-mq.service';

@Global()
@Module({
  exports: [RabbitMqService],
  providers: [RabbitMqService],
})
export class RabbitMqModule {}
