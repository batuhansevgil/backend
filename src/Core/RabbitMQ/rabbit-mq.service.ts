import { Injectable } from '@nestjs/common';
import { connect, Channel } from 'amqplib';
import configuration from 'src/configuration';
import { QueueName } from 'src/Modules/Types/global.types';

export type activeChannel = {
  channel: any;
  queue: string;
};
@Injectable()
export class RabbitMqService {
  public activeChannels: Array<activeChannel> = [];
  private config = configuration();
  constructor() {
    this.rabbitMqConnect().then();
  }
  public channel: Channel = null;
  public connection;

  async rabbitMqConnect() {
    const connection = await connect(this.config.rabbitmq.host);

    this.connection = connection;
    const channel = await connection.createChannel();
    // await channel.assertQueue(this.config.rabbitmq.queue || 'signalQueue', { durable: true });
    this.channel = channel;
    return channel;
  }
  async assertQueue(queueName) {
    this.channel.assertQueue(queueName);
  }
  async createChannel(queue) {
    const queueOnChannel = this.activeChannels.find((i) => i.queue === queue);

    if (queueOnChannel) {
      return queueOnChannel.channel;
    } else {
      const channel = this.connection.createChannel();
      this.activeChannels.push({ channel, queue });
      return channel;
    }
  }
  async getQueue(channel: Channel, queueName: QueueName | string): Promise<any> {
    return channel.get(queueName, { noAck: true });
  }
  async sendQueue(queueName: QueueName, Item: Buffer) {
    await this.channel.assertQueue(queueName);
    return this.channel.sendToQueue(queueName, Item);
  }
  async checkQueueItem(queueName) {
    await this.channel.assertQueue(queueName);

    const { messageCount } = await this.channel.checkQueue(queueName);
    return messageCount;
  }
}
