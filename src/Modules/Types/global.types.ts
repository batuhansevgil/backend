export enum EventName {
  setSignal = 'setSignal',
  getSignal = 'getSignal',
}
export enum QueueName {
  signalQueue = 'DATAQueue',
  currentSettings = 'CMDQueue',
}
