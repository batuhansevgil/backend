import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { Sensor } from '../sensor/schema/sensor.schema';
import { ChannelService } from './channel.service';
import { ActiveChannelDTO } from './dto/ActiveChannel.dto';
import { CreateChannelDto } from './dto/create-channel.dto';
import { UpdateChannelDto } from './dto/update-channel.dto';

@Controller('channel')
export class ChannelController {
  constructor(private readonly channelService: ChannelService) {}

  @Post()
  create(@Body() createChannelDto: CreateChannelDto) {
    return this.channelService.create(createChannelDto);
  }

  @Post('currentSettings')
  currentSettings(@Body() channelSettings) {
    console.log('aaa');
    return this.channelService.sendSettingsToQueue(channelSettings);
  }

  @Patch('channelSendSensor')
  channelSendSensor(@Body() activeChannel: ActiveChannelDTO) {
    // return thiss.channelService.channelSendSensor(activeChannel);
  }

  @Get()
  findAll() {
    return this.channelService.findAll().populate('activeSensor');
  }
  w;
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.channelService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateChannelDto: UpdateChannelDto) {
    return this.channelService.update(id, updateChannelDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.channelService.remove(+id);
  }
}
