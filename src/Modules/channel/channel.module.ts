import { Module } from '@nestjs/common';
import { ChannelController } from './channel.controller';
import { Channel, ChannelSchema } from './schema/channel.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { ChannelService } from './channel.service';
import { EmbeddedModule } from '../embedded/embedded.module';
import { SensorModule } from '../sensor/sensor.module';

@Module({
  imports: [MongooseModule.forFeature([{ name: Channel.name, schema: ChannelSchema }]), EmbeddedModule, SensorModule],
  controllers: [ChannelController],
  providers: [ChannelService],
})
export class ChannelModule {}
