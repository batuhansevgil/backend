import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Channel } from 'diagnostics_channel';
import { Model } from 'mongoose';
import { EmbeddedService } from '../embedded/embedded.service';
import { Sensor } from '../sensor/schema/sensor.schema';
import { SensorService } from '../sensor/sensor.service';
import { QueueName } from '../Types/global.types';
import { ActiveChannelDTO } from './dto/ActiveChannel.dto';
import { CreateChannelDto } from './dto/create-channel.dto';
import { QueueItemSend } from './dto/QueueSendItem';
import { UpdateChannelDto } from './dto/update-channel.dto';
import { ChannelDocument } from './schema/channel.schema';

@Injectable()
export class ChannelService {
  constructor(
    @InjectModel(Channel.name) private readonly channelModel: Model<ChannelDocument>,
    private readonly embeddedService: EmbeddedService,
    private readonly sensorService: SensorService,
  ) {}
  async create(createChannelDto: CreateChannelDto) {
    const preSaveEntity = new this.channelModel(createChannelDto);
    await preSaveEntity.save();
    return preSaveEntity;
  }

  findAll() {
    return this.channelModel.find({}).populate('activeSensor');
  }

  findOne(id: number) {
    return `This action returns a #${id} channel`;
  }

  async update(_id: string, updateChannelDto: UpdateChannelDto) {
    return this.channelModel.updateOne({ _id }, updateChannelDto);
  }

  remove(id: number) {
    return `This action removes a #${id} channel`;
  }
  async sendSettingsToQueue(settings) {
    await this.embeddedService.sendDataQueue(QueueName.currentSettings, Buffer.from(JSON.stringify(settings), 'utf8'));
  }

  async freezeChannel(activeChannelDto: ActiveChannelDTO) {
    const result = await this.channelModel.updateOne({ id: activeChannelDto.channelId }, activeChannelDto);
    await this.channelSendSensor(activeChannelDto, true);
  }
  releaseChannel(id: string) {
    this.channelModel.updateOne({ id }, { activeSensor: null });
  }
  previewChannel(id: string) {
    this.channelModel.updateOne({ id }, { activeSensor: 'PREVIEW' });
  }

  async channelSendSensor(activeChannel: ActiveChannelDTO, isActive: boolean) {
    const { name: channel } = await this.channelModel.findById(activeChannel.channelId).populate('activeSensor');
    const sensor = await this.sensorService.findOne(activeChannel.sensorId);
    const sendItem: QueueItemSend = {
      sensor,
      channel,
      isActive,
    };
    // const sendItem = {
    //   isActive,
    // };
    console.log('sendItem :>> ', sendItem);
    await this.embeddedService.sendDataQueue(QueueName.currentSettings, Buffer.from(JSON.stringify(sendItem), 'utf8'));
  }
}
