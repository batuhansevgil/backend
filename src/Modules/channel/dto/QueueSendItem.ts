import { Sensor } from 'src/Modules/sensor/schema/sensor.schema';

export class QueueItemSend {
  sensor: Sensor;
  channel: string;
  isActive: boolean;
}
