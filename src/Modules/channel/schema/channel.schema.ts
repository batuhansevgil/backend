import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId, Types } from 'mongoose';
import { Sensor, SensorSchema } from 'src/Modules/sensor/schema/sensor.schema';

export type ChannelDocument = Channel & Document;

@Schema({
  timestamps: true,
  _id: true,
  strict: false,
})
export class Channel {
  @Prop()
  name: string;
  @Prop({ type: Types.ObjectId, ref: () => Sensor, path: '_id', default: null })
  activeSensor: Types.ObjectId;
}

export const ChannelSchema = SchemaFactory.createForClass(Channel);
