import { Module } from '@nestjs/common';
import { RabbitMqModule } from 'src/Core/RabbitMQ/rabbit-mq.module';
import { EmbeddedService } from './embedded.service';

@Module({
  imports: [RabbitMqModule],
  providers: [EmbeddedService],
  exports: [EmbeddedService],
})
export class EmbeddedModule {}
