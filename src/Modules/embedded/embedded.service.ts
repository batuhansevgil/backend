import { Injectable } from '@nestjs/common';
import { randomInt } from 'crypto';
import { RabbitMqService } from 'src/Core/RabbitMQ/rabbit-mq.service';
import { QueueName } from '../Types/global.types';

@Injectable()
export class EmbeddedService {
  constructor(public readonly rabbitMqService: RabbitMqService) {}
  public counter = 0;
  getRandomData() {
    return randomInt(500);
  }

  async getDataOnQueue(channel, queue: QueueName | string) {
    return this.rabbitMqService.getQueue(channel, queue);
  }
  getChannel() {
    return this.rabbitMqService.channel;
  }
  async checkQueueItemCount(queueName) {
    return this.rabbitMqService.checkQueueItem(queueName);
  }

  async sendDataQueue(queue: QueueName, content: Buffer) {
    this.rabbitMqService.sendQueue(queue, content);
  }
  async assertQueue(queueName) {
    await this.rabbitMqService.assertQueue(queueName);
  }
  async createChannel(queue) {
    return this.rabbitMqService.createChannel(queue);
  }
}
