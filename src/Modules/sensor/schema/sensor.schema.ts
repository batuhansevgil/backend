import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
import { Channel } from 'src/Modules/channel/schema/channel.schema';

export type SensorDocument = Sensor & Document;

@Schema({
  timestamps: true,
  _id: true,
  strict: false,
})
export class Sensor {
  @Prop()
  sensorName: string;
  @Prop({ type: Types.ObjectId, ref: () => Channel, path: '_id' })
  channelName: Types.ObjectId;

  @Prop()
  description: string;

  @Prop()
  inputType: string;
  @Prop()
  voltageRange: string;
  @Prop()
  rangeUnit: string;
  @Prop()
  antiAliasingFilter: string;
  @Prop()
  excitation: string;
  @Prop()
  excitationUnit: string;
  @Prop()
  firstPointVoltage: string;
  @Prop()
  firstPointUnit: string;
  @Prop()
  samplingRate: string;
  @Prop()
  decimalPoint: string;
  @Prop()
  twoPointCalibrationUnit: string;

  @Prop()
  secondPointVoltage: string;
  @Prop()
  secondPointUnit: string;

  @Prop()
  scaleFactor: string;

  @Prop()
  offset: string;
}

export const SensorSchema = SchemaFactory.createForClass(Sensor);
