import { WebSocketServer, SubscribeMessage, MessageBody, WebSocketGateway, ConnectedSocket, OnGatewayDisconnect } from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { RabbitMqService } from 'src/Core/RabbitMQ/rabbit-mq.service';
import { EventName } from '../Types/global.types';
import { SensorService } from './sensor.service';

@WebSocketGateway({ cors: true, connectTimeout: 0 })
export class SensorGateway {
  constructor(private readonly rabbitMqService: RabbitMqService, private readonly sensorService: SensorService) {}

  async afterInit() {
    await this.rabbitMqService.rabbitMqConnect();

    Promise.all([
      this.sensorService.boardcast(this.server, 'CH2'),
      this.sensorService.boardcast(this.server, 'CH1'),
      this.sensorService.boardcast(this.server, 'CH3'),
    ]);
  }
  // async handleDisconnect(client: Socket) {
  //   console.log('client :>>  disconnect', client.id, new Date().getMilliseconds());
  // }
  // async handleConnection(client: Socket) {
  //   console.log('client :>>  connect ', client.id);
  // }

  @WebSocketServer() server: Server;

  @SubscribeMessage(EventName.setSignal)
  handleMessage(@ConnectedSocket() client: Socket, @MessageBody() payload: any): any {
    console.log('geldi');
    this.sensorService.boardcast(this.server, payload.channel);
  }

  @SubscribeMessage('joinroom')
  joinRoom(@ConnectedSocket() client: Socket, @MessageBody() payload: any) {
    console.log('Join room req  ', payload);
    // TODO payload.channelName added and change and to declared type

    client.join(payload);
    this.sensorService.boardcast(this.server, payload);
  }
  @SubscribeMessage('leaveroom')
  leaveRoom(@ConnectedSocket() client: Socket, @MessageBody() payload: any) {
    // TODO payload.channelName added and change and to declared type

    client.leave(payload.room);
    console.log('LEAVE :>> ', this.server.to(payload).allSockets());
  }
}
