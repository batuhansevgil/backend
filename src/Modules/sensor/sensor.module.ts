import { Module } from '@nestjs/common';
import { SensorService } from './sensor.service';
import { SensorController } from './sensor.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Sensor, SensorSchema } from './schema/sensor.schema';
import { SensorGateway } from './sensor.gateway';
import { EmbeddedModule } from '../embedded/embedded.module';
import { RabbitMqModule } from 'src/Core/RabbitMQ/rabbit-mq.module';

@Module({
  imports: [MongooseModule.forFeature([{ name: Sensor.name, schema: SensorSchema }]), EmbeddedModule, RabbitMqModule],
  controllers: [SensorController],
  providers: [SensorService, SensorGateway],
  exports: [SensorService],
})
export class SensorModule {}
