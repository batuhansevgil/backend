import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Channel } from 'amqplib';
import { Model } from 'mongoose';
import { Server, Socket } from 'socket.io';
import { EmbeddedService } from '../embedded/embedded.service';
import { QueueName } from '../Types/global.types';
import { CreateSensorDto } from './dto/create-sensor.dto';
import { UpdateSensorDto } from './dto/update-sensor.dto';
import { Sensor, SensorDocument } from './schema/sensor.schema';

@Injectable()
export class SensorService {
  constructor(
    @InjectModel(Sensor.name)
    private readonly sensorModel: Model<SensorDocument>,
    private readonly embeddedService: EmbeddedService,
  ) {}
  async create(createSensorDto: CreateSensorDto) {
    const preSaveEntity = new this.sensorModel(createSensorDto);
    await preSaveEntity.save();
    return preSaveEntity;
  }

  findAll() {
    return this.sensorModel.find({}).populate('channelName');
  }

  findOne(id: string) {
    return this.sensorModel.findById(id);
  }

  update(id: number, updateSensorDto: UpdateSensorDto) {
    return this.sensorModel.updateOne({ id }, updateSensorDto);
  }

  remove(id: number) {
    return this.sensorModel.deleteOne({ id });
  }
  async sendQueue(queue: QueueName, content: Buffer) {
    await this.embeddedService.sendDataQueue(queue, content);
  }

  async emitRoom(server: Server, room: string, eventName: string, content: any) {
    await server.to(room).emit(eventName, content);
  }
  async boardcast(server: Server, room: string) {
    console.log('room :>> ', room);

    const channel: Channel = await this.embeddedService.createChannel(room);
    await channel.assertQueue(room, { durable: false });
    //! this scpoe is  activeListining size 0=>1 Override queue.
    //  const isRoomListining = await server.to(room).allSockets();
    //  if(isRoomListining.size==1){
    //   channel.purgeQueue(room);
    // }
    //!

    let bucket = [];
    while (true) {
      const isRoomListining = await server.to(room).allSockets();
      const queueItemCount = await channel.checkQueue(room);
      if (isRoomListining.size && queueItemCount.messageCount) {
        const queueItem = await new Promise((resolve) => {
          const item = this.embeddedService.getDataOnQueue(channel, room);
          resolve(item);
        }).then((value) => value);
        debugger;
        const sendItem = await this.contentParser(queueItem['content']);
        bucket.push(sendItem);

        if (bucket.length === 10) {
          await this.emitRoom(server, room, 'getSignal', bucket);
          bucket = [];
        } else if (queueItemCount.messageCount < 10) {
          await this.emitRoom(server, room, 'getSignal', bucket);
          bucket = [];
        }
      }
    }
  }

  async contentParser(queueContent) {
    if (typeof queueContent === 'undefined') console.log('undefiinnnnnnnd');
    const queueItem = await queueContent.toString();
    // TODO Refactor this func => byte by byte DECLARE byte length
    const yAxis = +queueItem.substring(0, 3);
    const xAxis = +queueItem.substring(3, 16);
    const sendItem = { xAxis, yAxis };
    return sendItem;
  }
  //TODO calculate bucket
  // async calculateBucket(queueItemCount: number) {
  //   const MILLISECONDS_IN_SECONDS = 1000;
  //   let BUCKET_SIZE = 0;
  //   if (queueItemCount <= 10) {
  //     BUCKET_SIZE = 1;
  //   } else if (queueItemCount <= 1000) {
  //     BUCKET_SIZE = 10;
  //   } else if (queueItemCount <= 10000) {
  //     BUCKET_SIZE = 20;
  //   }
  //   return BUCKET_SIZE;
  // }
}
