import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type ViewDocument = View & Document;

@Schema({
  timestamps: true,
  _id: true,
})
export class View {
  @Prop()
  key: string;
  @Prop()
  items: Array<Record<any, any>>;
}

export const ViewSchema = SchemaFactory.createForClass(View);
