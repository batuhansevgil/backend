import { Module } from '@nestjs/common';
import { ViewService } from './view.service';
import { ViewController } from './view.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { View, ViewSchema } from './schema/view.schema';

@Module({
  imports: [MongooseModule.forFeature([{ name: View.name, schema: ViewSchema }])],
  controllers: [ViewController],
  providers: [ViewService],
})
export class ViewModule {}
