import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateViewDto } from './dto/create-view.dto';
import { UpdateViewDto } from './dto/update-view.dto';
import { View, ViewDocument } from './schema/view.schema';

@Injectable()
export class ViewService {
  /**
   *
   */
  constructor(
    @InjectModel(View.name)
    private readonly viewModel: Model<ViewDocument>,
  ) {}
  async create(createViewDto: CreateViewDto) {
    const view = new this.viewModel(createViewDto);
    await view.save();
  }

  async findAll() {
    const formValues = await this.viewModel.find({}).lean();
    const extractOfArray = {};
    formValues.map((item) => {
      const { key, items } = item;
      extractOfArray[key] = items;
    });
    return extractOfArray;
  }

  findOne(id: number) {
    return `This action returns a #${id} view`;
  }

  update(id: number, updateViewDto: UpdateViewDto) {
    return `This action updates a #${id} view`;
  }

  remove(id: number) {
    return `This action removes a #${id} view`;
  }
}
