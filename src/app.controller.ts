import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import configuration from './configuration';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return configuration().db.mongouri;
  }
}
