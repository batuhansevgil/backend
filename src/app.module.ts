import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { MongoModule } from './Core/Mongo/mongo.module';
import { EmbeddedModule } from './Modules/embedded/embedded.module';
import configuration from './configuration';
import { ViewModule } from './Modules/view/view.module';
import { SensorModule } from './Modules/sensor/sensor.module';
import { ChannelModule } from './Modules/channel/channel.module';

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true, load: [configuration] }), MongoModule, EmbeddedModule, ViewModule, SensorModule, ChannelModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
