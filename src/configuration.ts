export default () => ({
  app: {
    port: process.env.APP_PORT || 3000,
  },
  db: { mongouri: process.env.MONGOURI },
  rabbitmq: {
    queue: process.env.RABBITMQ_QUEUE || 'signalQueue',
    host: process.env.RABBITMQ_HOST || 'amqp://localhost:5672',
  },

  websocket: {
    port: process.env.WEBSOCKET_PORT || 3000,
  },
});
