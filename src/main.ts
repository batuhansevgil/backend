import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  await app.listen(3000);
}
bootstrap();
//TODO : webSocket event name getSignal olarak değiştir frontend ise sendSignal dinlesin
//TODO : frequncy olan herşeyi signal olarak değiştir.
//TODO : rabbitMQ ya queue Send yapacak fonksiyonu yaz.
//TODO : rabbitMQ connect fonksiyonu parametrik yap?
